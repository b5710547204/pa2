package ku.util;

import java.util.EmptyStackException;

/**
 * Stack for keep items.
 * @author Patinya Yongyai
 * @version 19.02.2015
 * @param <T> is a type parameter
 */
public class Stack<T>{
	private T[] items ;

	/**
	 * Create Stack class with capacity.
	 * @param capacity is the maximum number of elements that this Stack can hold.
	 */
	public Stack(int capacity){
		if(capacity >= 0)
			this.items = (T[]) new Object[capacity];
		else
			this.items = (T[]) new Object[0];
	}

	/**
	 * Check capacity of stack.
	 * @return the maximum number of elements that this Stack can hold. Return -1 if unknown or infinite.
	 */
	public int capacity(){
		if(items == null)
			return -1;
		else
			return items.length;

	}

	/**
	 * 
	 * @return true if stack is empty
	 */
	public boolean isEmpty(){
		if(items.length == 0||items[0] == null)
			return true;
		return false;
	}

	/**
	 * 
	 * @return true if stack is full
	 */
	public boolean isFull(){
		if(items.length == 0 || items[items.length-1] != null)
			return true;

		return false;

	}

	/**
	 * 
	 * @return the item on the top of the stack. Return null if stack is empty
	 */
	public T peek(){
		int index = items.length-1;
		if(isEmpty())
			return null;
		else{
			while(items[index]==null){
				index--;
			}
			T keepItem = items[index];
			return keepItem;
		}
	}

	/**
	 * 
	 * @return the item on the top of the stack. Return Throws: EmptyStackException if stack is empty
	 */
	public T pop() {
		int index = items.length-1;
		if(isEmpty())
			throw new EmptyStackException(); 
		else{
			while(items[index]==null){
				index--;
			}
			T keepItem = items[index];
			items[index] = null;
			return keepItem;
		}
	}

	/**
	 * Add object to stack.
	 * @param obj is item for push to stack
	 */
	public void push( T obj ){

		if(obj == null)
			throw new IllegalArgumentException();
		if(!isFull())
			items[size()] = obj;
	}

	/**
	 * Check size in stack.
	 * @return the number of items in the stack. Returns 0 if the stick is empty.
	 */
	public int size(){
		if(isEmpty())
			return 0;

		int count = 0;
		for(int i = 0 ; i < items.length ; i++){
			if(items[i] != null)
				count++;
		}
		return count;
	}


}
