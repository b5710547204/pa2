package ku.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * We can iterate over all the elements in the collection.
 * @author Patinya Yongyai
 * @version 19.02.2015
 * @param <T> is a type parameter
 */

public class ArrayIterator<T> implements Iterator<T>{
	private T[] array;
	private int position = 0;

	/**
	 * 
	 * @param array is array for create this class
	 */
	public ArrayIterator(T[] array){
		this.array = array;
	}

	/**
	 *  @return true if next() can return information
 	 */
	public boolean hasNext() {
		for(int i=position;i<array.length;i++){
			if(array[i]!=null)
				return true;
		}
		return false;
	}

	/**
	 * If there are no more elements, throws NoSuchElementException.
	 * @return data in array if hasNext() can return true.
	 */
	public T next() {
		T keepObject=null;
		if(hasNext()){
			while(array[position] == null){
				position++;
			}
			keepObject = array[position];
			position++;
		}else{
			throw new NoSuchElementException();
		}
		return keepObject;
	}

	/**
	 * Remove most recent element returned by next() from the array by setting it to null.
	 */
	public void remove(){
		this.array[position-1] = null;
	}


}
